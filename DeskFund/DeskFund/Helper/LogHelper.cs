﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace DeskFund
{
    public class LogHelper
    {
        private static string logPath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + "\\Logs\\";

        public static void viewLog()
        {
            if (File.Exists(logPath + DateTime.Now.ToString("yyyy-MM-dd") + ".txt"))
            {
                Process.Start(logPath + DateTime.Now.ToString("yyyy-MM-dd") + ".txt");
            }
        }

        public static string Debug(string text)
        {
            return logMessage("[调试] ", text);
        }

        public static string Info(string text)
        {
            return logMessage("[信息] ", text);
        }

        public static string Warning(string text)
        {
            return logMessage("[警告] ", text);
        }

        public static string Error(string text)
        {
            return logMessage("[错误] ", text);
        }

        public static string Fatal(string text)
        {
            return logMessage("[严重] ", text);
        }

        private static string logMessage(string status, string text)
        {
            string message = DateTime.Now.ToString("HH:mm:ss ") + status + text;

            if (!Directory.Exists(logPath))
                Directory.CreateDirectory(logPath);
            FileStream fs = new FileStream(logPath + DateTime.Now.ToString("yyyy-MM-dd") + ".txt", FileMode.Append, FileAccess.Write, FileShare.Read);
            StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.Default);
            sw.WriteLine(message);
            sw.Close();
            fs.Close();

            return message;
        }
    }
}
